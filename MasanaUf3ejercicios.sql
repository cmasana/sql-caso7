/* 
 * ADMINISTRACIÓN DE USUARIOS, ROLES Y PERFILES
 * Descripción: ASIX-DAW1B UF3 RA1 CAS7
 * Autor: Masana
 * Fecha: 03/04/19
 */

/* 1. Crea un rol ROLPRACTICA1 con los privilegios necesarios para conectarse a la base de datos,
crear tablas y vistas e insertar datos en la tabla EMP de HR (o el usuario que hayas utilizado en el caso 4). */
CREATE ROLE ROLPRACTICA1; -- Creación de rol
GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW TO ROLPRACTICA1; -- Otorgar privilegios de sistema
GRANT INSERT ON EMPLOYEES TO ROLPRACTICA1; -- Otorgar privilegio sobre un objeto determinado (tabla employees)

/* 2. Crea un usuario USRPRACTICA1 con el tablespace USERS por defecto y averigua que cuota se le ha asignado por defecto
en el mismo. Sustitúyela por una cuota de 1M. */
CREATE USER USRPRACTICA1 IDENTIFIED BY alumne DEFAULT TABLESPACE USERS;

/* 2.1. Muestra un listado de los tablespaces y sus cuotas por defecto */
SELECT TABLESPACE_NAME, ROUND(SUM(BYTES)/1024/1024,0) AS "CUOTA EN MB"
FROM DBA_FREE_SPACE
WHERE TABLESPACE_NAME NOT LIKE 'TEMP%'
GROUP BY TABLESPACE_NAME;

/* 2.2. Modificar la cuota de USRPRACTICA1 */
ALTER USER USRPRACTICA1 IDENTIFIED BY alumne QUOTA 1M ON USERS;

/* 3. Modifica el usuario USRPRACTICA1 para que tenga cuota 0 en el tablespace SYSTEM. */
ALTER USER USRPRACTICA1 IDENTIFIED BY alumne QUOTA 0M ON SYSTEM;

/* 4. Concede a USRPRACTICA1 el ROLPRACTICA1. */
GRANT ROLPRACTICA1 TO USRPRACTICA1;

/* 5. Concede a USRPRACTICA1 el privilegio de crear tablas e insertar datos en el esquema de cualquier usuario.
Prueba el privilegio. Comprueba si puede modificar la estructura o eliminar las tablas creadas. */
GRANT CREATE ANY TABLE TO USRPRACTICA1;
GRANT INSERT ANY TABLE TO USRPRACTICA1;

/* Listar privilegios otorgados a determinados roles */
SELECT * FROM ROLE_SYS_PRIVS
WHERE ROLE LIKE 'ROL%'; -- Privilegios de sistema

SELECT * FROM ROLE_TAB_PRIVS
WHERE ROLE LIKE 'ROL%'; -- Privilegios sobre tablas

/* Si creamos una tabla con una PK en el schema de otro usuario es necesario otorgar privilegios para crear índices.
Si no lo hacemos, developer mostrará un error de privilegios insuficientes */
GRANT CREATE ANY INDEX TO USRPRACTICA1;

/* Listar privilegios de un determinado rol y/o usuario */
SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE LIKE 'ROL%'
OR GRANTEE LIKE 'USR%'
ORDER BY GRANTEE ASC;

-- Comprobaciones en el pdf adjunto

/* 6. Concede a USRPRACTICA1 el privilegio de leer la tabla DEPT de SCOTT con la posibilidad de que lo pase a su vez a
terceros usuarios. */
GRANT SELECT ON SCOTT.DEPT TO USRPRACTICA1 WITH GRANT OPTION;

/* 7. Comprueba que USRPRACTICA1 puede realizar todas las operaciones previstas en el rol. */
-- Ver en el pdf adjunto

/* 8. Quita a USRPRACTICA1 el privilegio de crear vistas. Comprueba que ya no puede hacerlo. */
REVOKE CREATE VIEW FROM ROLPRACTICA1; -- El privilegio de crear vistas se encuentra asignado en ROLPRACTICA1

/* 9. Crea un perfil NOPARESDECURRAR que limita a dos el número de minutos de inactividad permitidos en una sesión. */
CREATE PROFILE NOPARESDECURRAR LIMIT IDLE_TIME 2;
-- DROP PROFILE NOPARESDECURRAR CASCADE; -- Para desactivar el perfil. Se añade cascade por si algún usuario lo tiene asignado

/* 10. Activa el uso de perfiles en ORACLE. */
ALTER SYSTEM SET RESOURCE_LIMIT=TRUE; -- FALSE los desactiva

/* 11. Asigna el perfil creado a USRPRACTICA1 y comprueba su correcto funcionamiento. */
ALTER USER USRPRACTICA1 PROFILE NOPARESDECURRAR;

/* Perfiles asignados a un determinado usuario */
SELECT USERNAME, ACCOUNT_STATUS, PROFILE FROM DBA_USERS
WHERE USERNAME = 'USRPRACTICA1';

/* 12. Crea un perfil CONTRASEÑASEGURA especificando que la contraseña caduca mensualmente y sólo se permiten tres intentos
fallidos para acceder a la cuenta. En caso de superarse, la cuenta debe quedar bloqueada indefinidamente. */
CREATE PROFILE CONTRASEÑASEGURA LIMIT PASSWORD_LIFE_TIME 30 FAILED_LOGIN_ATTEMPTS 3;

/* 13. Asigna el perfil creado a USRPRACTICA1 y comprueba su funcionamiento. Desbloquea posteriormente al usuario. */
ALTER USER USRPRACTICA1 PROFILE CONTRASEÑASEGURA;

/* Desbloquear cuenta */
ALTER USER USRPRACTICA1 ACCOUNT UNLOCK;

-- Comprobación en el PDF adjunto

/* 14. Consulta qué usuarios existen en tu base de datos. */
SELECT * FROM DBA_USERS;

/* 15. Elige un usuario concreto y consulta qué cuota tiene sobre cada uno de los tablespaces. */
SELECT TABLESPACE_NAME, USERNAME, ROUND(MAX_BYTES/1024/1024) "CUOTA EN MB" FROM DBA_TS_QUOTAS
WHERE USERNAME = 'USRPRACTICA1';

/* Existe también una vista que muestra la cuota del usuario actual */
SELECT * FROM USER_TS_QUOTAS;

/* 16. Elige un usuario concreto y muestra qué privilegios de sistema tiene asignados. */
SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'USRPRACTICA1';

/* 17. Elige un usuario concreto y muestra qué privilegios sobre objetos tiene asignados. */
SELECT * FROM DBA_TAB_PRIVS
WHERE GRANTEE = 'USRPRACTICA1';

/* Muestra los privilegios sobre objetos que tiene el usuario actual */
SELECT * FROM USER_TAB_PRIVS;

/* 18. Consulta qué roles existen en tu base de datos. */
SELECT * FROM DBA_ROLES;

/* 19. Elige un rol concreto y consulta qué usuarios lo tienen asignado. */
SELECT * FROM DBA_ROLE_PRIVS
WHERE GRANTED_ROLE = 'ROLPRACTICA1';

/* Muestra los roles asignados al usuario actual */
SELECT * FROM SESSION_ROLES;

/* 20. Elige un rol concreto y averigua si está compuesto por otros roles o no. */
SELECT * FROM ROLE_ROLE_PRIVS
WHERE GRANTED_ROLE = 'CONNECT';

/* 21. Consulta qué perfiles existen en tu base de datos. */
SELECT PROFILE FROM DBA_PROFILES
GROUP BY PROFILE;

/* 22. Elige un perfil y consulta qué límites se establecen en el mismo. */
SELECT * FROM DBA_PROFILES
WHERE PROFILE = 'NOPARESDECURRAR';

/* 23. Muestra los nombres de los usuarios que tienen limitado el número de sesiones concurrentes. */
SELECT DU.USERNAME, DU.PROFILE, DP.RESOURCE_NAME, DP.LIMIT FROM DBA_USERS DU
INNER JOIN DBA_PROFILES DP
ON DU.PROFILE = DP.PROFILE
AND DP.RESOURCE_NAME LIKE 'SESS%'
AND DP.LIMIT NOT LIKE 'UNL%';